import Vue from 'vue'
import Vuex from 'vuex'

import { auth } from './modules/auth'
import { products } from './modules/products'
import { cart } from './modules/cart'
import { favorites } from './modules/favorites'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    products,
    cart,
    favorites
  },
  getters: {}
})
