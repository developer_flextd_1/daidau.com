const state = {
  productsInCart: localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : []
}

const getters = {
  // tokens
  GET_ITEMS: state => state.productsInCart
}

const mutations = {
  ADD_ITEM: (state, payload) => {
    const existingProduct = state.productsInCart.find(p => payload.product.id === p.id)

    if (existingProduct) {
      existingProduct.quantity += payload.quantity
    } else {
      // if the product does not exist, create a new product object
      const newProduct = {
        ...payload.product,
        quantity: payload.quantity
      }
      state.productsInCart.push(newProduct)
    }

    // update the cart in local storage too
    localStorage.setItem('cart', JSON.stringify(state.productsInCart))
  },
  UPDATE_ITEM: (state, payload) => {
    const existingProduct = state.productsInCart.find(p => payload.id === p.id)
    existingProduct.quantity = payload.quantity

    // update the cart in local storage too
    localStorage.setItem('cart', JSON.stringify(state.productsInCart))
  },
  REMOVE_ITEM: (state, payload) => {
    const indexOfProduct = state.productsInCart.findIndex(p => p.id === payload)
    state.productsInCart.splice(indexOfProduct, 1)

    // update the cart in local storage too
    localStorage.setItem('cart', JSON.stringify(state.productsInCart))
  }
}
// todo: add action to update cart in server when the user logs in
const actions = {}

export const cart = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
