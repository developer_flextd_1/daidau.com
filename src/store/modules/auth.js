const state = {
  token: localStorage.getItem('token') ? localStorage.getItem('token') : null,
  default_token: localStorage.getItem('default_token') ? localStorage.getItem('default_token') : null,
  user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null
}

const getters = {
  // tokens
  GET_TOKEN: state => state.token ? state.token : state.default_token,
  GET_DEFAULT_TOKEN: state => state.default_token,
  GET_USER_TOKEN: state => state.token,

  // user
  GET_USER: state => state.user
}

const mutations = {
  // tokens
  SET_TOKEN: (state, payload) => {
    state.token = payload
    localStorage.setItem('token', payload)
  },
  REMOVE_TOKEN: (state) => {
    state.token = null
    localStorage.removeItem('token')
  },
  SET_DEFAULT_TOKEN: (state, payload) => {
    state.default_token = payload
    localStorage.setItem('default_token', payload)
  },

  // user
  SET_USER: (state, payload) => {
    state.user = payload
    localStorage.setItem('user', JSON.stringify(payload))
  },
  REMOVE_USER: (state) => {
    state.user = null
    state.token = null
    localStorage.removeItem('user')
    localStorage.removeItem('token')
  }
}

const actions = {
  async LOGIN (state, payload) {
    try {
      const loginForm = new FormData()
      loginForm.append('email', payload.email)
      loginForm.append('password', payload.password)
      // Login using provided form data
      const response = await fetch(process.env.VUE_APP_LOGIN_AP, { method: 'POST', body: loginForm })
      // Resolve the promise with the result (the calling function deals with the result)
      return Promise.resolve(response)
    } catch (error) {
      // on error, reject the promise
      return Promise.reject(error)
    }
  },

  async LOGIN_DEFAULT_ACCOUNT () {
    try {
      // Login with default account
      const loginForm = new FormData()
      loginForm.append('email', process.env.VUE_APP_DEFAULT_LOGIN_EMAIL)
      loginForm.append('password', process.env.VUE_APP_DEFAULT_LOGIN_PASS)
      const response = await fetch(process.env.VUE_APP_LOGIN_AP, { method: 'POST', body: loginForm })
      // if fetch failed, throw an error
      if (!response.ok) {
        throw new Error('Error logging in - ' + response.statusText)
      }
      // wait for the data in JSON format
      const data = await response.json()

      console.log('Log in with default account successful')

      return Promise.resolve(data.token)
    } catch (error) {
      // on error, reject the promise
      return Promise.reject(error)
    }
  },

  async RETRIEVE_USER_DATA ({ rootGetters }) {
    // when the user successfully logs in, (and its not the default user) fetch the user details
    if (!rootGetters['auth/GET_USER_TOKEN']) {
      return Promise.reject(new Error('Error getting user token'))
    }

    try {
      const response = await fetch(process.env.VUE_APP_USER_DETAILS_AP, {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + rootGetters['auth/GET_USER_TOKEN']
        }
      })
      // return the resolved promise (the calling function deals with the result)
      return Promise.resolve(response)
    } catch (error) {
      Promise.reject(error)
    }
  },

  async SIGN_UP (state, payload) {
    const formData = new FormData()
    formData.append('email', payload.email)
    formData.append('password', payload.password)
    formData.append('confirm_password', payload.password)
    formData.append('name', payload.name)

    try {
      const response = await fetch(process.env.VUE_APP_SIGNUP_AP, { method: 'POST', body: formData })
      // return the resolver promise (the calling function deals with the result)
      return Promise.resolve(response)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export const auth = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
