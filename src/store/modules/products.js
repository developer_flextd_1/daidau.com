const state = {
  categories: localStorage.getItem('categories') ? JSON.parse(localStorage.getItem('categories')) : null,
  products: localStorage.getItem('products') ? JSON.parse(localStorage.getItem('products')) : null
}

const getters = {
  GET_CATEGORIES: state => state.categories,
  GET_PRODUCTS: state => state.products
}

const mutations = {
  SET_CATEGORIES: (state, payload) => {
    state.categories = payload
    localStorage.setItem('categories', JSON.stringify(payload))
  },

  SET_PRODUCTS: (state, payload) => {
    state.products = payload
    localStorage.setItem('products', JSON.stringify(payload))
  }
}

const actions = {
  async RETRIEVE_CATEGORIES ({ rootGetters, commit }) {
    try {
      const response = await fetch(process.env.VUE_APP_CATEGORIES_AP, {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + rootGetters['auth/GET_TOKEN']
        }
      })

      if (!response.ok) {
        throw new Error('Could not retrieve categories from server - ' + response.statusText)
      }

      const data = await response.json()
      // commit the categories to store and local storage
      commit('SET_CATEGORIES', data.data)
      // return the result
      return Promise.resolve(data.data)
    } catch (error) {
      // on error, reject the promise
      return Promise.reject(error)
    }
  },

  async RETRIEVE_PRODUCTS ({ rootGetters, commit }) {
    try {
      const response = await fetch(process.env.VUE_APP_PRODUCTS_AP, {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + rootGetters['auth/GET_TOKEN']
        }
      })

      if (!response.ok) {
        throw new Error('Could not retrieve products from server - ' + response.statusText)
      }

      const data = await response.json()
      // parse the pictures
      data.data.forEach(product => {
        product.pictures = JSON.parse(product.pictures)
        product.technical = JSON.parse(product.technical)
        product.description = new DOMParser().parseFromString(product.description, 'text/html').documentElement.textContent
      })
      // commit the products to store and local storage
      commit('SET_PRODUCTS', data.data)
      // return the result
      return Promise.resolve(data.data)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export const products = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
