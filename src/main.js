import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import store from './store'
import VueRouter from 'vue-router'

import Home from './components/views/Home.vue'
import ProductView from './components/views/ProductView.vue'
import Shop from './components/views/Shop.vue'
import Cart from './components/cart/Cart.vue'
import Login from './components/auth/Login.vue'
import Signup from './components/auth/Signup.vue'
import Checkout from './components/views/Checkout.vue'

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFacebook, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons'
import { faHeart, faShoppingCart, faCartPlus, faUser, faTimes, faSortDown, faSearch, faBars, faHome, faTag, faStore } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faFacebook, faInstagram, faTwitter, faHeart, faShoppingCart, faTag, faStore, faCartPlus, faUser, faTimes, faSortDown, faSearch, faBars, faHome)

Vue.component('font-awesome-icon', FontAwesomeIcon)
// Font awesome end

Vue.config.productionTip = false

const routes = [
  { path: '/', component: Home },
  { path: '/product-view', component: ProductView },
  { path: '/cart', component: Cart },
  { path: '/login', component: Login },
  { path: '/signup', component: Signup },
  { path: '/shop', component: Shop, name: 'shop' },
  { path: '/checkout', component: Checkout }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
